import 'dart:io';
import 'package:path/path.dart';
import 'dart:async';
import 'package:path_provider/path_provider.dart';
import 'package:sqflite/sqflite.dart';

import '../models/user.dart';

class DatabaseHelper {
  static final DatabaseHelper _instance = new DatabaseHelper.internal();

  factory DatabaseHelper() => _instance;

  static Database? _db ;
  final String tableUser = "User";
  final String columnEmail = "email";
  final String columnPassword = "password";

  Future<Database?> get db async {
    if (_db != null) {
      return _db;
    }
    _db = await initDb();
    return _db;
  }

  DatabaseHelper.internal();

  initDb() async {
    Directory documentDirectory = await getApplicationDocumentsDirectory();
    String path = join(documentDirectory.path, "main.db");
    var ourDb = await openDatabase(path, version: 1, onCreate: _onCreate);
    return ourDb;
  }

  void _onCreate(Database db, int version) async {
    await db.execute(
        "CREATE TABLE User(id INTEGER PRIMARY KEY, email TEXT, username TEXT, password TEXT)");
  }

  //insertion
  Future<int> saveUser(User user) async {
    var dbClient = await db;
    print(user.email);
    int res = await dbClient!.insert("User", user.toJson());
    return res;
  }

  Future<bool?> selectUser(User user) async {
    var dbClient = await db;
    List<Map> maps = await dbClient!.query(tableUser,
        columns: [columnEmail, columnPassword],
        where: "$columnEmail = ? and $columnPassword = ?",
        whereArgs: [user.email, user.password]);
    if (maps.length > 0) {
      return true;
    } else {
      return false;
    }
  }
}