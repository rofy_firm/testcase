import 'dart:convert';

import 'package:dio/dio.dart';
import 'package:majootestcase/models/movie_detail_response.dart';
import 'package:majootestcase/models/movie_response.dart';
import 'package:majootestcase/services/dio_config_service.dart' as dioConfig;
import 'package:majootestcase/utils/constant.dart';
import 'package:majootestcase/utils/error_helper.dart';

class ApiServices{

  Future<dynamic> getMovieList() async {
    try {
      var dio = await dioConfig.dio();
      Response<String>? response  = await dio?.get(Api.TRENDING,
          queryParameters: {
            "page": 1,
          },
      );
      MovieResponse movieResponse = MovieResponse.fromJson(jsonDecode(response!.data!));
      return movieResponse;
    } on DioError catch(e) {
      return ErrorHelper.extractApiError(e);
    }
  }

  Future<dynamic> getMovieDetail(String mediaType, String id) async {
    try {
      var dio = await dioConfig.dio();
      Response<String>? response  = await dio?.get("$mediaType/$id");
      MovieDetailResponse movieDetailResponse = MovieDetailResponse.fromJson(jsonDecode(response!.data!));
      return movieDetailResponse;
    } on DioError catch(e) {
      return ErrorHelper.extractApiError(e);
    }
  }
}