import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:majootestcase/models/movie_response.dart';
import 'package:majootestcase/services/api_service.dart';

part 'home_bloc_state.dart';

class HomeBlocCubit extends Cubit<HomeBlocState> {
  HomeBlocCubit() : super(HomeBlocInitialState());

  void fetching_data() async {
    emit(HomeBlocInitialState());
    emit(HomeBlocLoadingState());
    ApiServices apiServices = ApiServices();
   var movieResponse = await apiServices.getMovieList();
    if(movieResponse is String){
      emit(HomeBlocErrorState(movieResponse));
    }else{
      emit(HomeBlocLoadedState(movieResponse.results!));
    }
  }
}
