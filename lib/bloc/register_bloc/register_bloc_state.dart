part of 'register_bloc_cubit.dart';

abstract class RegisterBlocState extends Equatable {
  const RegisterBlocState();

  @override
  List<Object> get props => [];
}

class RegisterBlocInitialState extends RegisterBlocState { }

class RegisterBlocSuccessState extends RegisterBlocState {
  final data;

  RegisterBlocSuccessState(this.data);

  @override
  List<Object> get props => [data];
}

class RegisterBlocErrorState extends RegisterBlocState {
  final error;

  RegisterBlocErrorState(this.error);

  @override
  List<Object> get props => [error];
}
