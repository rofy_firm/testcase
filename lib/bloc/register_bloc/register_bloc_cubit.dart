import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';

import '../../database/database_helper.dart';
import '../../models/user.dart';

part 'register_bloc_state.dart';

class RegisterBlocCubit extends Cubit<RegisterBlocState> {
  RegisterBlocCubit() : super(RegisterBlocInitialState());

  void register_user(User user){
    var db = new DatabaseHelper();
    var res = db.saveUser(user);
    if (res != null){
      emit(RegisterBlocSuccessState("Register ${user.email} berhasil"));
    } else emit(RegisterBlocErrorState("Terjadi Kesalahan"));
  }
}
