import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:majootestcase/models/movie_detail_response.dart';

import '../../services/api_service.dart';

part 'detail_bloc_state.dart';

class DetailBlocCubit extends Cubit<DetailBlocState> {
  DetailBlocCubit() : super(DetailBlocInitialState());

  void fetching_data(String mediaType, String id) async {
    emit(DetailBlocLoadingState());
    ApiServices apiServices = ApiServices();
    var movieDetailResponse = await apiServices.getMovieDetail(mediaType, id);
    if(movieDetailResponse is String){
      emit(DetailBlocErrorState(movieDetailResponse));
    }else{
      emit(DetailBlocLoadedState(movieDetailResponse));
    }
  }
}
