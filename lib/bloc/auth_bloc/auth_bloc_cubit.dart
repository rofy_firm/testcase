import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:majootestcase/models/user.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../../database/database_helper.dart';

part 'auth_bloc_state.dart';

class AuthBlocCubit extends Cubit<AuthBlocState> {
  AuthBlocCubit() : super(AuthBlocInitialState());

  void fetch_history_login() async{
    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
    bool? isLoggedIn = sharedPreferences.getBool("is_logged_in");
    if(isLoggedIn== null){
      emit(AuthBlocLoginState());
    }else{
      if(isLoggedIn){
        emit(AuthBlocLoggedInState(isLoggedIn));
      }else{
        emit(AuthBlocLoginState());
      }
    }
  }

  void login_user(User user) async{
    DatabaseHelper db = new DatabaseHelper();
    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
    var loggedIn = await db.selectUser(user);

    if (loggedIn!) {
      await sharedPreferences.setBool("is_logged_in",loggedIn);
      emit(AuthBlocLoggedInState(loggedIn));
    } else {
      emit(AuthBlocErrorState("Login Gagal, periksa kembali inputan anda"));
    }
  }
}
