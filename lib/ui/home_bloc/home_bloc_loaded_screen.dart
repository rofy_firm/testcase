import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:majootestcase/models/movie_response.dart';

import '../../bloc/detail_bloc/detail_bloc_cubit.dart';
import '../detail_bloc/detail_bloc_screen.dart';

class HomeBlocLoadedScreen extends StatelessWidget {
   final List<Results> data;

  const HomeBlocLoadedScreen({Key? key, required this.data}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Beranda"),
      ),
      body: SafeArea(
        child: GridView.builder(
        itemCount: data.length,
        itemBuilder: (context, index) {
          return movieItemWidget(data[index], context);
        }, gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(mainAxisSpacing: 16.0, crossAxisCount: 2, childAspectRatio: 0.5),
    ),
      ),
    );
  }

  Widget movieItemWidget(Results data, context){
    return InkWell(
      onTap: () {
        Navigator.push(
          context,
          MaterialPageRoute(
            builder: (_) => BlocProvider(
              create: (context) => DetailBlocCubit()..fetching_data(data.mediaType!, data.id.toString()),
              child: DetailBlocScreen(mediatype: data.mediaType!, id: data.id.toString(),),
            ),
          ),
        );
      },
      child: Card(
        shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.all(Radius.circular(25.0)
            )
        ),
        child: Column(
          mainAxisSize: MainAxisSize.max,
          children: [
            Padding(
              padding: EdgeInsets.all(25),
              child: Image.network("https://image.tmdb.org/t/p/original${data.posterPath}", scale: 0.2,),
            ),
            Padding(
              padding: EdgeInsets.symmetric(vertical: 8, horizontal: 20),
              child: Text(data.title?? data.name!, textDirection: TextDirection.ltr,
                style: TextStyle(fontSize: 16, fontWeight: FontWeight.bold),),
            )
          ],
        ),
      ),
    );
  }
}
