import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:majootestcase/bloc/register_bloc/register_bloc_cubit.dart';

import '../../bloc/home_bloc/home_bloc_cubit.dart';
import '../../common/widget/custom_button.dart';
import '../../common/widget/text_form_field.dart';
import '../../models/user.dart';
import '../home_bloc/home_bloc_screen.dart';

class RegisterPage extends StatefulWidget {
  @override
  State<RegisterPage> createState() => _RegisterPageState();
}

class _RegisterPageState extends State<RegisterPage> {
  final _emailController = TextController(initialValue: '');
  final _usernameController = TextController(initialValue: '');
  final _passwordController = TextController(initialValue: '');
  GlobalKey<FormState> formKey = new GlobalKey<FormState>();

  bool _isObscurePassword = true;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: BlocListener<RegisterBlocCubit, RegisterBlocState>(
        listener: (context, state) {
          if (state is RegisterBlocSuccessState) {
            Fluttertoast.showToast(
                msg: state.data,
                toastLength: Toast.LENGTH_LONG,
                gravity: ToastGravity.BOTTOM,
                timeInSecForIosWeb: 1,
                backgroundColor: Colors.white24,
                textColor: Colors.white,
                fontSize: 16.0
            );
            Navigator.push(
              context,
              MaterialPageRoute(
                builder: (_) =>
                    BlocProvider(
                      create: (context) =>
                      HomeBlocCubit()
                        ..fetching_data(),
                      child: HomeBlocScreen(),
                    ),
              ),
            );
          }
        },
        child: SafeArea(
          child: SingleChildScrollView(
            child: Padding(
              padding: EdgeInsets.only(
                  top: 75, left: 25, bottom: 25, right: 25),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Text(
                    'Registrasi',
                    style: TextStyle(
                      fontSize: 24,
                      fontWeight: FontWeight.bold,
                      // color: colorBlue,
                    ),
                  ),
                  Text(
                    'Silahkan melakukan registrasi',
                    style: TextStyle(
                      fontSize: 15,
                      fontWeight: FontWeight.w400,
                    ),
                  ),
                  SizedBox(
                    height: 9,
                  ),
                  _form(),
                  SizedBox(
                    height: 50,
                  ),
                  CustomButton(
                    text: 'Register',
                    onPressed: () {
                      handleRegister(context);
                    },
                    height: 100,
                  ),
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }

  Widget _form() {
    return Form(
      key: formKey,
      child: Column(
        children: [
          CustomTextFormField(
            context: context,
            controller: _emailController,
            isEmail: true,
            hint: 'Example@123.com',
            label: 'Email',
            validator: (val) {
              final pattern = new RegExp(r'([\d\w]{1,}@[\w\d]{1,}\.[\w]{1,})');
              if (val != null)
                return pattern.hasMatch(val) ? null : '“Masukkan e-mail yang valid';
            },
          ),
          CustomTextFormField(
            context: context,
            controller: _usernameController,
            hint: 'username',
            label: 'Username',
          ),
          CustomTextFormField(
            context: context,
            label: 'Password',
            hint: 'password',
            controller: _passwordController,
            isObscureText: _isObscurePassword,
            suffixIcon: IconButton(
              icon: Icon(
                _isObscurePassword
                    ? Icons.visibility_off_outlined
                    : Icons.visibility_outlined,
              ),
              onPressed: () {
                setState(() {
                  _isObscurePassword = !_isObscurePassword;
                });
              },
            ),
          ),
        ],
      ),
    );
  }

  void handleRegister(BuildContext context) async {
    final String _email = _emailController.value;
    final String _password = _passwordController.value;
    final String _username = _usernameController.value;
    if (formKey.currentState?.validate() == true &&
        _email != null &&
        _password != null && _username != null
    ) {
      formKey.currentState?.save();
      var user = new User(
          email: _email,
          password: _password,
          userName: _username);
      BlocProvider.of<RegisterBlocCubit>(context).register_user(user);
    }
  }
}
