import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:majootestcase/bloc/detail_bloc/detail_bloc_cubit.dart';
import 'package:majootestcase/ui/detail_bloc/detail_bloc_loaded_screen.dart';

import '../extra/error_screen.dart';
import '../extra/loading.dart';

class DetailBlocScreen extends StatelessWidget {
  const DetailBlocScreen({Key? key, required this.mediatype, required this.id}) : super(key: key);

  final String mediatype;
  final String id;

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<DetailBlocCubit, DetailBlocState>(
        builder: (context, state)
        {
          if(state is DetailBlocLoadedState)
          {
            return DetailBlocLoadedScreen(data: state.data);
          }
          else if(state is DetailBlocLoadingState)
          {
            return LoadingIndicator();
          }

          else if(state is DetailBlocInitialState)
          {
            return Scaffold();
          }

          else if(state is DetailBlocErrorState)
          {
            return ErrorScreen(message: state.error,
              retry: () {
                BlocProvider.of<DetailBlocCubit>(context).fetching_data(mediatype, id);
              },);
          }

          return Center(child: Text(
              kDebugMode?"state not implemented $state": ""
          ));
        });
  }
}
