import 'package:flutter/material.dart';
import 'package:majootestcase/models/movie_detail_response.dart';

class DetailBlocLoadedScreen extends StatelessWidget {
  final MovieDetailResponse data;

  const DetailBlocLoadedScreen({Key? key, required this.data}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Detail"),
      ),
      body: SingleChildScrollView(
        child: SafeArea(
          child: Padding(
            padding: EdgeInsets.all(10),
            child: Container(
              width: double.infinity,
              child: Column(
                mainAxisSize: MainAxisSize.max,
                children: [
                  Padding(
                    padding: EdgeInsets.all(25),
                    child: Image.network("https://image.tmdb.org/t/p/original${data.posterPath}", scale: 0.2),
                  ),
                  movieSubMenu("Judul"),
                  movieContent(data.title?? data.name!),
                  movieSubMenu("Tanggal Rilis"),
                  movieContent(data.releaseDate?? data.firstAirDate!),
                  movieSubMenu("Rating"),
                  movieContent(data.voteAverage.toString()?? ""),
                  movieSubMenu("Overview"),
                  movieContent(data.overview?? ""),
                  movieSubMenu("Popularitas"),
                  movieContent(data.popularity.toString()?? ""),
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }

  Padding movieContent(String? content) {
    return Padding(
      padding: EdgeInsets.symmetric(vertical: 8, horizontal: 20),
      child: Text(content!, textDirection: TextDirection.ltr,
        style: TextStyle(fontSize: 16),),
    );
  }

  Padding movieSubMenu(String? content) {
    return Padding(
      padding: EdgeInsets.symmetric(vertical: 8, horizontal: 20),
      child: Text(content!, textDirection: TextDirection.ltr,
        style: TextStyle(fontSize: 16, fontWeight: FontWeight.bold),),
    );
  }
}
