class Preference {
  static const USER_INFO = "user-info";
}

class Api {
  static const BASE_URL = "https://api.themoviedb.org/3/";
  static const API_KEY = "aa6c32dd159431d80ee9f9fd383b7eb1";
  static const TRENDING = "trending/all/day";
  static const REGISTER = "/register";
}

class Font {
}

class ScreenUtilConstants {
  static const width = 320.0;
  static const height = 640.0;
}
